# Messy data store

## Docker images
`dockerPullCounter.csv` is the number of downloads of some Docker images from https://hub.docker.com/.  

`dockerPullCounter_2021-12-08_2022-06-29.csv` is a copy of old data which was captured once per hour in order to try to understand why we have so many downloads of the `squashtest/squash-tm` image.

`dockerPullCounter_2022-06-29_2023-01-30.csv` is a copy of less old data which was captured once per day. (The docker image `opentestfactory/orchestrator` has been deleted on 2022-07-20. I did not have the time to update the script after that, so the data was lost for a month.)

The script capturing this data is https://gitlab.com/m4711/gitlab-tools/-/blob/main/dockerPullCounter.py and https://gitlab.com/m4711/gitlab-tools/-/blob/main/dockerPullCounter.sh.
